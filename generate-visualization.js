#!/usr/bin/env node

const fs = require('fs-extra');
const path = require('path');
const { getStaticReportHTML } = require('./index');

const CURRENT_FOLDER = process.cwd();
const DESTINATION_FOLDER = path.join(CURRENT_FOLDER, '.wrm-analyzer-reports');

function print(msg) {
    console.log(msg);
}

function getWRMReport(currentFolder, reportUrl) {
    return JSON.parse(fs.readFileSync(path.join(currentFolder, reportUrl), 'utf8'));
}

function ensureDestinationFolder(destinationFolder) {
    if (!fs.existsSync(destinationFolder)) {
        fs.mkdirSync(destinationFolder);
    }
}

(async () => {
    const [,, ...args] = process.argv;
    const [reportPath, baseUrl] = args;

    if (!reportPath || !baseUrl) {
        throw new Error('WRMDependencyAnalyzer Error: "reportPath" and "baseUrl" have to be specified.');
    }

    const WRMReport = getWRMReport(CURRENT_FOLDER, reportPath);
    const staticReportContent = await getStaticReportHTML(WRMReport, baseUrl);
    const staticReportFilePath = path.join(DESTINATION_FOLDER, 'static-report.html');

    print('Drawing a beautiful interface.');

    ensureDestinationFolder(DESTINATION_FOLDER);

    fs.writeFileSync(staticReportFilePath, staticReportContent);

    print(`Your report is ready. You can find it here "${staticReportFilePath}"`);
})();
