# 1.1.0

## Changed
* Updated dependencies to latest versions.

## Fixed
* All dependencies used to generate the static report html page are now `devDependencies` instead of `dependencies`, making the package significantly lighter.
