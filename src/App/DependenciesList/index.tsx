import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Size from '../Size';

import { ISizesReportDescriptor, WebresourceKey } from '../types';
import { getSizesReport } from '../../utils/data';
import Table, { ITableRow } from '../Table/index';

interface IDependenciesListProps {
    content: Array<WebresourceKey>;
    columns: Array<ColumnKeys>;
    entryPointIdx?: number | null;
    asyncChunkIdx?: number;
}

export enum ColumnKeys {
    webresource = 'webresource',
    css = 'css',
    js = 'js',
    total = 'total',
}

export default class DependenciesList extends Component<IDependenciesListProps> {
    static defaultProps = {
        columns: [ColumnKeys.webresource, ColumnKeys.css, ColumnKeys.js, ColumnKeys.total],
    };

    get tableHead() {
        return [
            {
                key: ColumnKeys.webresource,
                content: 'Webresource',
                isSortable: true,
            },
            {
                key: ColumnKeys.css,
                content: 'CSS',
                isSortable: true,
            },
            {
                key: ColumnKeys.js,
                content: 'JS',
                isSortable: true,
            },
            {
                key: ColumnKeys.total,
                content: 'Total',
                isSortable: true,
            },
        ].filter(this.filterColumns);
    }

    render() {
        const sizesReport = getSizesReport();
        const { content } = this.props;
        const dependenciesList = this.getWebresourcesAsTableRows(content, sizesReport);

        return <Table head={this.tableHead} rows={dependenciesList} />;
    }

    getWebresourcesAsTableRows = (webresources: Array<WebresourceKey>, sizesDict: ISizesReportDescriptor) => {
        const { entryPointIdx, asyncChunkIdx } = this.props;


        return webresources.map(webresourceKey => {
            const sizes = sizesDict.all[webresourceKey] || sizesDict.direct[webresourceKey] || {};

            const resourceUrl = asyncChunkIdx
                ? `/entry/${entryPointIdx}/chunk/${asyncChunkIdx}/resource/${webresourceKey}`
                : `/entry/${entryPointIdx}/resource/${webresourceKey}`;

            return [
                {
                    key: ColumnKeys.webresource,
                    value: webresourceKey,
                    content: entryPointIdx ? <Link to={resourceUrl}>{webresourceKey}</Link> : webresourceKey,
                },
                {
                    key: ColumnKeys.css,
                    value: sizes.css,
                    content: <Size content={sizes.css} />,
                },
                {
                    key: ColumnKeys.js,
                    value: sizes.js,
                    content: <Size content={sizes.js} />,
                },
                {
                    key: ColumnKeys.total,
                    value: sizes.total,
                    content: <Size content={sizes.total} />,
                },
            ].filter(this.filterColumns);
        });
    };

    filterColumns = (cell: ITableRow<ColumnKeys>) => {
        const { columns } = this.props;

        return columns.includes(cell.key);
    };
}
