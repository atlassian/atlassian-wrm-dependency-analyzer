import React from 'react'

import './style.less';

interface ISizeProps {
    content?: number | string;
}

const Size = ({content}: ISizeProps) => {
    if (typeof content === 'string') {
        content = parseInt(content, 10);
    }

    return (
        <span className="webresource-size">{content ? (<>{(content / 1024).toFixed(1)}<span>kb</span></>) : '–'}</span>
    )
};

export default Size;