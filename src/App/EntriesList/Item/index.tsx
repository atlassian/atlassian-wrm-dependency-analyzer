import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import PropertiesTable from '../../PropertiesTable';
import Size from '../../Size';

import { EntryPointName } from '../../types';
import { getExternalSizesReport } from '../../../utils/data';

import './style.less';

interface IEntryItemProps {
    idx: number;
    name: EntryPointName;
}

export default class EntryItem extends Component<IEntryItemProps> {
    render() {
        const externalSizesReport = getExternalSizesReport();

        if (!externalSizesReport) {
            return null;
        }

        const { name, idx } = this.props;
        const sizes = externalSizesReport[idx];

        const propertiesToDisplay = [
            {
                name: 'External size',
                values: [sizes.externalSize.css, sizes.externalSize.js, sizes.externalSize.total],
            },
            {
                name: 'Direct size',
                values: [sizes.directSize.css, sizes.directSize.js, sizes.directSize.total],
            },
            {
                name: 'Indirect size',
                values: [sizes.indirectSize.css, sizes.indirectSize.js, sizes.indirectSize.total],
            },
            {
                name: 'Uniq size',
                values: [sizes.uniqueSize.css, sizes.uniqueSize.js, sizes.uniqueSize.total],
            },
        ];

        return (
            <div className="entry-item">
                <h2 className="entry-item__name">
                    <Link to={`/entry/${idx}`}>{name}</Link>
                </h2>
                <PropertiesTable content={propertiesToDisplay} valueWrapper={(val: number) => <Size content={val} />} />
            </div>
        );
    }
}
