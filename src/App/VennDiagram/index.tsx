import React, { Component, ReactElement } from 'react'
import * as d3 from 'd3';
import * as venn from 'venn.js';

import { WebresourceKey } from '../types';
import { getSizesReport } from '../../utils/data';

import './style.less';
import Size from '../Size/index';

interface DiagramSet {
    sets: Array<WebresourceKey>;
    label: string;
    figure: number;
    size: number;
}

interface IVennDiagramProps {
    id: string;
    sets: Array<DiagramSet>;
    width?: number;
    height?: number;
}

interface IVennDiagramState {
    legendContent: Array<ReactElement>
}

export default class VennDiagram extends Component<IVennDiagramProps, IVennDiagramState> {
    palette = [
        '#0052CC',
        'rgb(255, 86, 48)'
    ]
    defaultWidth = 200;
    defaultHeight = 300;
    chart: any;

    state = {
        legendContent: []
    }

    componentDidMount() {
        this.drawChart();
    }

    render() {
        const { legendContent } = this.state;

        return (
            <div className="venn-diagram">
                <div id={this.getId()} />
                {legendContent ? (
                    <div className="venn-diagram__legend">
                        {legendContent.map(legend => <span className="venn-diagram__legend-item">{legend}</span>)}
                    </div>
                ) : null}
            </div>
        );
    }

    drawChart() {
        const { sets } = this.props;
        const id = this.getId();
        let colorIdx = 0; 

        const chart = venn.VennDiagram()
            .width(this.width)
            .height(this.height)
            .colours(() => {
                colorIdx++;

                return this.palette[colorIdx % this.palette.length]
            });

        this.chart = d3.select(`#${id}`)
            .datum(sets)
            .call(chart);

        this.chart.selectAll('.venn-circle path')
            .style('fill-opacity', .7)
            .style('stroke-width', 0.5)
            .style('stroke-opacity', 0.5)
            .style('stroke', 'fff');
       

        this.fillLegend();
        this.removeChartLabels();
        this.bindChartEvents();
    }

    fillLegend() {
        const sizeReport = getSizesReport();
        const legendContent: Array<ReactElement> = [];

        this.chart.selectAll('.venn-circle')
            .each(function(this: SVGElement) {
                const circle = d3.select(this);
                const color = circle.select('path').style('fill');
                const webresourceKey = circle.select('text tspan').text();

                legendContent.push(
                    <>
                        <div className="venn-diagram__legend-color" style={{ backgroundColor: color }} />
                        <div className="venn-diagram__legend-text">{webresourceKey}</div>
                        (<Size content={sizeReport.direct[webresourceKey].total} />)
                    </>
                );
            });

            this.setState({
                legendContent
            });
    }

    removeChartLabels() {
        this.chart.selectAll('.venn-area text').remove();
    }

    bindChartEvents() {
        const chart = this.chart;

        this.chart.selectAll('g')
            .on('mouseover', function(this:SVGElement, d: any) {
                const selection = d3.select(this);
                
                venn.sortAreas(chart, d);

                selection.select('path')
                    .style('stroke-width', 1)
                    .style('fill-opacity', d.sets.length == 1 ? .8 : 0)
                    .style('stroke-opacity', 1);
            })

            .on('mouseout', function(this: SVGElement, d: any) {
                const selection = d3.select(this);

                selection.select('path')
                    .style('stroke-width', 1)
                    .style('fill-opacity', d.sets.length == 1 ? .8 : 0)
                    .style('stroke-opacity', 1);
            });
    }

    getId() {
        const escapedId = this.props.id.replace(/\.|\+|\:/g, '_');

        return `venn_${escapedId}`;
    }

    get height() {
        return this.props.height || this.defaultHeight;
    }

    get width() {
        return this.props.width || this.defaultWidth;
    }
}