import React, { Component } from 'react';

import './style.less';

export default class FixedContainer extends Component {
    _contentRefCb = (ref: HTMLDivElement) => {
        if (!ref) {
            return;
        }

        const offsets = ref.getBoundingClientRect();
    
        this.setState({
            contentHeight: offsets.height
        });
    }

    state = {
        contentHeight: 0
    }

    render() {
        const { children } = this.props;
        const { contentHeight } = this.state;

        return (
            <div className="fixed-container">
                <div style={{ height: `${contentHeight}px` }} />
                <div className="fixed-container__content" ref={this._contentRefCb}>
                    {children}
                </div>
            </div>
        );
    }
}