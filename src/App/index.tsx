import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import EntriesList from './EntriesList';
import Entry from './Entry';
import AsyncChunk from './AsyncChunk';
import Webresource from './Webresource';

import { getEntryPointsReport } from '../utils/data';

import './style.less';

class App extends Component {
    render() {
        const entryPointReport = getEntryPointsReport();

        if (!entryPointReport) {
            return null;
        }

        return (
            <div className="app">
                <Route path="/" exact render={() => <EntriesList content={entryPointReport} />} />
                <Route
                    path="/entry/:entryPointIdx"
                    exact
                    render={({ match }) => {
                        const { entryPointIdx } = match.params;

                        return <Entry entryPointIdx={parseInt(entryPointIdx, 10)} />;
                    }}
                />
                <Route
                    path="/entry/:entryPointIdx/chunk/:chunkIdx"
                    exact
                    render={({ match }) => {
                        const { entryPointIdx, chunkIdx } = match.params;

                        return <AsyncChunk entryPointIdx={parseInt(entryPointIdx, 10)} chunkIdx={parseInt(chunkIdx, 10)} />;
                    }}
                />
                <Route
                    path="/entry/:entryPointIdx/resource/:resourceName"
                    render={({ match }) => {
                        const { entryPointIdx, resourceName } = match.params;

                        return <Webresource entryPointIdx={entryPointIdx} resourceName={resourceName} />;
                    }}
                />
                <Route
                    path="/entry/:entryPointIdx/chunk/:chunkIdx/resource/:resourceName"
                    render={({ match }) => {
                        const { entryPointIdx, chunkIdx, resourceName } = match.params;

                        return (
                            <Webresource
                                chunkIdx={chunkIdx}
                                entryPointIdx={entryPointIdx}
                                resourceName={resourceName}
                            />
                        );
                    }}
                />
            </div>
        );
    }
}

export default App;
